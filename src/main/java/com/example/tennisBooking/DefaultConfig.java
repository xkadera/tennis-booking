package com.example.tennisBooking;

import com.example.tennisBooking.phonenumber.PhoneNumber;
import com.example.tennisBooking.phonenumber.PhoneNumberRepository;
import com.example.tennisBooking.player.Player;
import com.example.tennisBooking.player.PlayerRepository;
import com.example.tennisBooking.surface.Surface;
import com.example.tennisBooking.surface.SurfaceRepository;
import com.example.tennisBooking.tenniscourt.TennisCourt;
import com.example.tennisBooking.tenniscourt.TennisCourtRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * Default configuration. Adds basic data to database.
 *
 * @author Radim Kaděra
 */

@Configuration
public class DefaultConfig {
    @Bean
    CommandLineRunner commandLineRunner(TennisCourtRepository tennisRep,
                                        SurfaceRepository surfaceRep,
                                        PlayerRepository playerRep,
                                        PhoneNumberRepository phoneNumberRep) {
        return args -> {
            Surface grass = new Surface("Grass", 5);
            Surface clay = new Surface("Clay", 10);
            Surface hardSurface = new Surface("Hard Surface", 7);
            Surface artificialGrass = new Surface("Artificial grass", 15);
            surfaceRep.saveAll(List.of(grass, clay, hardSurface, artificialGrass));

            TennisCourt grassCourt = new TennisCourt(1, grass);
            TennisCourt court2 = new TennisCourt(2, clay);
            TennisCourt court3 = new TennisCourt(3, hardSurface);
            TennisCourt court4 = new TennisCourt(4, artificialGrass);
            TennisCourt court5 = new TennisCourt(5, grass);
            tennisRep.saveAll(List.of(grassCourt, court2, court3, court4, court5));

            PhoneNumber number = new PhoneNumber(123456789L);
            phoneNumberRep.save(number);

            Player player = new Player("Radim Kaděra", number);
            playerRep.save(player);
        };
    }

}
