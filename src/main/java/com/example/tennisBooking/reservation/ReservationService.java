package com.example.tennisBooking.reservation;

import com.example.tennisBooking.phonenumber.PhoneNumber;
import com.example.tennisBooking.phonenumber.PhoneNumberService;
import com.example.tennisBooking.player.MatchType;
import com.example.tennisBooking.player.Player;
import com.example.tennisBooking.player.PlayerService;
import com.example.tennisBooking.tenniscourt.TennisCourt;
import com.example.tennisBooking.tenniscourt.TennisCourtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Business layer for Tennis court.
 *
 * @author Radim Kaděra
 */
@Service
public class ReservationService {
    private final ReservationRepository reservationRepository;
    private final TennisCourtService tennisCourtService;
    private final PlayerService playerService;
    private final PhoneNumberService phoneNumberService;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, TennisCourtService tennisCourtService, PlayerService playerService, PhoneNumberService phoneNumberService) {
        this.reservationRepository = reservationRepository;
        this.tennisCourtService = tennisCourtService;
        this.playerService = playerService;
        this.phoneNumberService = phoneNumberService;
    }

    /**
     * Gets reservations made by Player with given Phone number
     *
     * @param phoneNumber given phone number
     * @return List of reservations
     */
    public List<Reservation> getReservationsByPhone(Long phoneNumber) {
        PhoneNumber curPhoneNumber = new PhoneNumber(phoneNumber);
        PhoneNumber phoneNumberFromDtb = phoneNumberService.getPhoneNumber(curPhoneNumber);
        return reservationRepository.findReservationsByPlayer_Phone_Id(phoneNumberFromDtb.getId());
    }

    /**
     * Gets reservations made on chosen Court number.
     *
     * @param courtNum given Court number
     * @return List of reservations
     */
    public List<Reservation> findReservationsByCourtNum(Integer courtNum) {
        TennisCourt tennisCourt = tennisCourtService.findCourtByNum(courtNum);
        return tennisCourt.getReservation();
    }

    /**
     * Creates new Reservation for Player on Tennis court.
     *
     * @param reservation body of reservation with start and end times.
     * @param playerId    id of player who is making reservation
     * @param courtNum    number of court to be reserved
     * @return price of reservation
     */
    public Double newReservation(Reservation reservation, Long playerId, Integer courtNum) {
        Player player = playerService.findPlayerById(playerId);
        TennisCourt tennisCourt = tennisCourtService.findCourtByNum(courtNum);
        reservation.setPlayer(player);
        reservation.setTennisCourt(tennisCourt);
        tennisCourt.addReservation(reservation);
        reservation.setMatchType(MatchType.SINGLES);

        reservationRepository.save(reservation);
        return reservation.price();
    }
}
