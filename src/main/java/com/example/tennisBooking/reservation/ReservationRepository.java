package com.example.tennisBooking.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Data access Layer for Reservation.
 *
 * @author Radim Kaděra
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    /**
     * Finds all reservations of player with given phone number id.
     *
     * @param phoneNumberId id of phone number
     * @return list of reservations
     */
    List<Reservation> findReservationsByPlayer_Phone_Id(Long phoneNumberId);
}
