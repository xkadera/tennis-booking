package com.example.tennisBooking.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * API layer for tennis courts.
 *
 * @author Radim Kaděra
 */
@RestController
@RequestMapping(path = "api/reservations")
public class ReservationController {
    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    /**
     * Gets reservations made by Player with given Phone number
     *
     * @param phoneNumber given phone number
     * @return List of reservations
     */
    @GetMapping("byPhoneNumber")
    public List<Reservation> getReservationsByPhone(
            @RequestParam Long phoneNumber) {
        return reservationService.getReservationsByPhone(phoneNumber);
    }

    /**
     * Gets reservations made on chosen Court number.
     *
     * @param courtNum given Court number
     * @return List of reservations
     */
    @GetMapping("byCourtNum")
    public List<Reservation> getReservationsByCourtNum(
            @RequestParam Integer courtNum) {
        return reservationService.findReservationsByCourtNum(courtNum);
    }

    /**
     * Creates new Reservation for Player on Tennis court.
     *
     * @param reservation body of reservation with start and end times.
     * @param playerId    id of player who is making reservation
     * @param courtNum    number of court to be reserved
     * @return price of reservation
     */
    @PostMapping
    public Double newReservation(@RequestBody Reservation reservation,
                                 @RequestParam Long playerId,
                                 @RequestParam Integer courtNum) {
        return reservationService.newReservation(reservation, playerId, courtNum);
    }
}
