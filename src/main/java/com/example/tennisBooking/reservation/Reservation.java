package com.example.tennisBooking.reservation;

import com.example.tennisBooking.player.MatchType;
import com.example.tennisBooking.player.Player;
import com.example.tennisBooking.tenniscourt.TennisCourt;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Class for representing reservation.
 *
 * @author Radim Kaděra
 */

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne()
    private Player player;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime startTime;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime endTime;

    @ManyToOne()
    @JsonBackReference
    private TennisCourt tennisCourt;

    @Enumerated(EnumType.STRING)
    private MatchType matchType;


    /**
     * Constructor for JPA.
     */
    public Reservation() {
    }

    public Long getId() {
        return id;
    }

    public Player getPlayer() {
        return player;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public TennisCourt getTennisCourt() {
        return tennisCourt;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setTennisCourt(TennisCourt tennisCourt) {
        this.tennisCourt = tennisCourt;
    }

    /**
     * Counts price of current reservation.
     *
     * @return price of reservation
     */
    public Double price() {
        double multiply = (matchType.compareTo(MatchType.SINGLES) == 0) ? 1 : 1.5;
        return Duration.between(startTime, endTime).toMinutes() * tennisCourt.getSurface().getMinutePrice() * multiply;
    }

    public void setMatchType(MatchType matchType) {
        this.matchType = matchType;
    }
}
