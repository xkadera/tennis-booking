package com.example.tennisBooking.surface;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Data access Layer for Surface.
 *
 * @author Radim Kaděra
 */
@Repository
public interface SurfaceRepository extends JpaRepository<Surface, Long> {
}
