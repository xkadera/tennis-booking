package com.example.tennisBooking.surface;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Class for representing Surface for tennis court. Surface has different minute pricing.
 *
 * @author Radim Kaděra
 */
@Entity
public class Surface {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String surfaceName;
    private int minutePrice;

    /**
     * Empty constructor for JPA.
     */
    public Surface() {
    }

    /**
     * Surface constructor for creating new surface with minute pricing.
     *
     * @param surfaceName name of surface
     * @param minutePrice minute pricing for reservation
     */
    public Surface(String surfaceName, int minutePrice) {
        this.surfaceName = surfaceName;
        this.minutePrice = minutePrice;
    }

    public Long getId() {
        return id;
    }

    public String getSurfaceName() {
        return surfaceName;
    }

    public int getMinutePrice() {
        return minutePrice;
    }
}
