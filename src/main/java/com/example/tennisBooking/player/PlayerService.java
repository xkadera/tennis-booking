package com.example.tennisBooking.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Business layer for Tennis court.
 *
 * @author Radim Kaděra
 */
@Service
public class PlayerService {
    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    /**
     * Finds player by its id from database.
     *
     * @param id id from database
     * @return wanted Player
     */
    public Player findPlayerById(Long id) {
        Optional<Player> player = playerRepository.findById(id);
        if (player.isEmpty()) {
            throw new IllegalArgumentException("Players id does not exist.");
        }
        return player.get();
    }
}
