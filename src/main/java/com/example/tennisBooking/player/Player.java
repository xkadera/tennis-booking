package com.example.tennisBooking.player;

import com.example.tennisBooking.phonenumber.PhoneNumber;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Class for representing player with unique Phone Number.
 *
 * @author Radim Kaděra
 */
@Entity
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    @OneToOne()
    private PhoneNumber phone;

    /**
     * Constructor for JPA.
     */
    public Player() {
    }

    /**
     * Constructor for new Player.
     *
     * @param name  name of new Player
     * @param phone phone number of new player
     */
    public Player(String name, PhoneNumber phone) {
        this.name = name;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PhoneNumber getPhone() {
        return phone;
    }
}
