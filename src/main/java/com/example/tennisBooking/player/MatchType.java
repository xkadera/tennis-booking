package com.example.tennisBooking.player;

/**
 * Enum for match types of tennis.
 *
 * @author Radim Kaděra
 */
public enum MatchType {
    SINGLES, DOUBLES
}
