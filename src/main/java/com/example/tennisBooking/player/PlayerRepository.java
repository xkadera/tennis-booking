package com.example.tennisBooking.player;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Data access Layer for Player.
 *
 * @author Radim Kaděra
 */
@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
}
