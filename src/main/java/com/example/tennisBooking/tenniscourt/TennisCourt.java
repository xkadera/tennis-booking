package com.example.tennisBooking.tenniscourt;

import com.example.tennisBooking.reservation.Reservation;
import com.example.tennisBooking.surface.Surface;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

/**
 * Class for representing Tennis Court.
 *
 * @author Radim Kaděra
 */

@Entity
public class TennisCourt {
    @Id
    private Integer courtNum;
    @OneToOne
    private Surface surface;
    @OneToMany(mappedBy = "tennisCourt", cascade = CascadeType.ALL)

    private List<Reservation> reservation;

    /**
     * Empty constructor for JPA.
     */
    public TennisCourt() {
    }

    /**
     * Constructor for creating new Tennis court with given existing surface.
     *
     * @param courtNum unique number of court
     * @param surface  surface of tennis court
     */
    public TennisCourt(Integer courtNum, Surface surface) {
        this.courtNum = courtNum;
        this.surface = surface;
    }

    public Integer getCourtNum() {
        return courtNum;
    }

    public Surface getSurface() {
        return surface;
    }

    public List<Reservation> getReservation() {
        return reservation;
    }

    public void addReservation(Reservation reservation) {
        this.reservation.add(reservation);
    }
}
