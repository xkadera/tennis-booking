package com.example.tennisBooking.tenniscourt;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Data access Layer for Tennis Court.
 *
 * @author Radim Kaděra
 */
@Repository
public interface TennisCourtRepository extends JpaRepository<TennisCourt, Integer> {
}

