package com.example.tennisBooking.tenniscourt;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * API layer for tennis courts.
 *
 * @author Radim Kaděra
 */

@RestController
@RequestMapping(path = "api/tennisCourts")
public class TennisCourtController {
    private final TennisCourtService tennisCourtService;

    public TennisCourtController(TennisCourtService tennisCourtService) {
        this.tennisCourtService = tennisCourtService;
    }

    /**
     * Gets all Tennis courts from database.
     *
     * @return List of Tennis courts.
     */
    @GetMapping
    public List<TennisCourt> getTennisCourts() {
        return tennisCourtService.getTennisCourts();
    }
}
