package com.example.tennisBooking.tenniscourt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Business layer for Tennis court.
 *
 * @author Radim Kaděra
 */

@Service
public class TennisCourtService {
    private final TennisCourtRepository tennisCourtRepository;

    @Autowired
    public TennisCourtService(TennisCourtRepository tennisCourtRepository) {
        this.tennisCourtRepository = tennisCourtRepository;
    }

    /**
     * Gets all Tennis courts from database.
     *
     * @return List of Tennis courts.
     */
    public List<TennisCourt> getTennisCourts() {
        return tennisCourtRepository.findAll();
    }


    /**
     * Find Tennis court by court number from database.
     *
     * @param courtNum tennis court number
     * @return Tennis Court
     */
    public TennisCourt findCourtByNum(Integer courtNum) {
        Optional<TennisCourt> tennisCourt = tennisCourtRepository.findById(courtNum);
        if (tennisCourt.isEmpty()) {
            throw new IllegalArgumentException("Tennis court " + courtNum + "does not exist.");
        }
        return tennisCourt.get();
    }

}
