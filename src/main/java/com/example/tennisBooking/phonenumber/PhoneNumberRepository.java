package com.example.tennisBooking.phonenumber;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Data access Layer for PhoneNumber.
 *
 * @author Radim Kaděra
 */
@Repository
public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {
    /**
     * If present returns Phone Number from database same as parameters given.
     *
     * @param areaCode area code of phone number
     * @param phoneNum phone number
     * @return Phone number if present
     */
    Optional<PhoneNumber> getPhoneNumberByAreaCodeAndPhoneNum(int areaCode, Long phoneNum);
}
