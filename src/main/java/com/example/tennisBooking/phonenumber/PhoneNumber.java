package com.example.tennisBooking.phonenumber;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Class representing full phone number.
 *
 * @author Radim Kaděra
 */
@Entity
public class PhoneNumber {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private int areaCode;

    private Long phoneNum;
    private static final int CZECHIA = 420;


    /**
     * Empty constructor for JPA
     */
    public PhoneNumber() {
    }

    /**
     * Constructor for full phone number with area code.
     *
     * @param areaCode area code of user
     * @param phoneNum phone number of user
     */
    public PhoneNumber(int areaCode, Long phoneNum) {
        this.areaCode = areaCode;
        this.phoneNum = phoneNum;
    }

    /**
     * Constructor takes default area code as CZECHIA.
     *
     * @param phoneNum phone number of user
     */
    public PhoneNumber(Long phoneNum) {
        this(CZECHIA, phoneNum);
    }

    public Long getId() {
        return id;
    }

    public int getAreaCode() {
        return areaCode;
    }

    public Long getPhoneNum() {
        return phoneNum;
    }
}
