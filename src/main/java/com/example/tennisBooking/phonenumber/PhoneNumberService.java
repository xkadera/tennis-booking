package com.example.tennisBooking.phonenumber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Business layer for Phone Number.
 *
 * @author Radim Kaděra
 */
@Service
public class PhoneNumberService {
    private final PhoneNumberRepository phoneNumberRepository;

    @Autowired
    public PhoneNumberService(PhoneNumberRepository phoneNumberRepository) {
        this.phoneNumberRepository = phoneNumberRepository;
    }

    /**
     * Gets instance of wanted Phone number from database.
     *
     * @param phoneNumber phone number of user
     * @return instance of phone number from database (with id from database)
     */
    public PhoneNumber getPhoneNumber(PhoneNumber phoneNumber) {
        Optional<PhoneNumber> curPhoneNumber = phoneNumberRepository
                .getPhoneNumberByAreaCodeAndPhoneNum(phoneNumber.getAreaCode(), phoneNumber.getPhoneNum());
        if (curPhoneNumber.isEmpty()) {
            throw new IllegalArgumentException("Phone number is not in database");
        }
        return curPhoneNumber.get();
    }
}
